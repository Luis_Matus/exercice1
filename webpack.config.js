// webpack.config.js
const VueLoaderPlugin = require('vue-loader/lib/plugin');
//var webpack = require('webpack');
//var helpers = require('./config/helpers');
const path = require('path');

module.exports = {
	mode: 'development', // “production” | “development” | “none”
	entry: {
		app 	:'./resources/js/app.js' //for vue
		//main 	:'./src/main.ts',     //for angular 
		//vendor	:'./src/vendor.ts'
	},
	resolve: {
		alias: {
			vue: 'vue/dist/vue.esm.js',
		},
		// Add `.ts` and `.tsx` as a resolvable extension.
    	//extensions: [".ts", ".tsx", ".js"]
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname+'/client/js/'),
		publicPath: path.resolve(__dirname+'/client/js/','build')
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				exclude: /node_modules/
			},
			/*{
		        test: /\.ts$/,
		        loaders: [
		          {
		            loader: 'awesome-typescript-loader',
		            options: { configFileName: helpers.root('src', 'tsconfig.json') }
		          } , 'angular2-template-loader'
		        ]
		    },*/
			{
				test: /\.html$/,
				loader: 'html-loader'

			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/
			},
			{
				test: /\.pug$/,
				loader: 'pug-plain-loader'
			},
			{
				test: /\.styl(us)?$/,
				use: [
					'vue-style-loader',
					'css-loader',
					'stylus-loader'
				]
			},/*
			{
		        test: /\.css$/,
		        exclude: helpers.root('src', 'app'),
		        loader: 'null-loader'
		    },
		    {
		        test: /\.css$/,
		        include: helpers.root('src', 'app'),
		        loader: 'raw-loader'
		    }*/
		    {
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			}

		]
	},

	plugins: [

		new VueLoaderPlugin(),
	],

	/*plugins: [
	    new webpack.ContextReplacementPlugin(
	      // The (\\|\/) piece accounts for path separators in *nix and Windows
	      /angular(\\|\/)core(\\|\/)@angular/,
	      helpers.root('./src'), // location of your src
	      {} // a map of your routes
	    )
	]*/

};

