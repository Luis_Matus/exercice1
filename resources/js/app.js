import Vue from 'vue';
import axios from 'axios';

window.Vue 	   = Vue;
window.Axios   = axios;

import 		     'bootstrap/dist/css/bootstrap.min.css';
let bootstrap  =  require('./bootstrap.js');
let css        = require('../stylus/main.styl');


import countriescomponent	from './components/CountriesComponent.vue';
import logincomponent		from './components/LoginComponent.vue';

const app = new Vue({
    el: '#app',
    components: {
		'countries-component'	: countriescomponent,
		'login-component'		: logincomponent,
	}
});

