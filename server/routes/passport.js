'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();
var cookieParser = require('cookie-parser');
var session = require('express-session');



var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;

app.get('/', function(req, res, next) {
	res.send('matus');
	res.render('login/index');
	/*res.render('pages/index', {user:
		req.user,
		url: req.url,
	});*/
});
//
app.get('/auth/account', ensureLoggedIn('/login'), function(req, res, next) {
	res.render('pages/loginProfiles', {
		user: req.user,
		url: req.url,
	});
});

app.get('/local', function(req, res, next) {
	res.render('pages/local', {
		user: req.user,
		url: req.url,
	});
});

app.get('/ldap', function(req, res, next) {
	res.render('pages/ldap', {
		user: req.user,
		url: req.url,
	});
});

app.get('/signup', function(req, res, next) {
	res.render('pages/signup', {
		user: req.user,
		url: req.url,
	});
});

app.post('/signup', function(req, res, next) {
	var User = app.models.User;

	var newUser = {};
	newUser.email = req.body.email.toLowerCase();
	newUser.username = req.body.username.trim();
	newUser.password = req.body.password;

	User.create(newUser, function(err, user) {
		if (err) {
			req.flash('error', err.message);
			return res.redirect('back');
		} else {
			// Passport exposes a login() function on req (also aliased as logIn())
			// that can be used to establish a login session. This function is
			// primarily used when users sign up, during which req.login() can
			// be invoked to log in the newly registered user.
			req.login(user, function(err) {
				if (err) {
					req.flash('error', err.message);
					return res.redirect('back');
				}
				return res.redirect('/auth/account');
			});
		}
	});
});

app.get('/login', function(req, res, next) {
	res.render('pages/login', {
		user: req.user,
		url: req.url,
	});
});
//

app.get('/auth/logout', function(req, res, next) {
	req.logout();
	res.redirect('/');
});
