var express = require('express');
var router = express.Router();


var http = require('http');
var io   = require('socket.io')(http);


var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();

// boot scripts mount components like REST API
boot(app, __dirname+'/..');
//sockey.io
router.io = require('socket.io');;


router.get('/', function (req, res) {

	res.render('countries/form');
});

router.post('/update', function (req, res) {

	var countries = app.models.countries;

	let idCountry  = req.body.country;
	let population = req.body.population;

	countries.upsertWithWhere({_id:idCountry}, {'population':population}, function(err,obj){
		if(err)
			res.send(err);

		res.send(JSON.stringify({ "status":"success"}));
		req.app.io.emit('countries', "server to client");

	});

});

module.exports = router;




